<?php
/**
 * Created by PhpStorm.
 * User: nkolosov
 * Date: 3/9/15
 * Time: 10:17 PM
 */

namespace app\controllers;


use yii\rest\ActiveController;

class UserController extends ActiveController {
    public $modelClass = 'app\models\User';
} 