<?php
/**
 * Created by PhpStorm.
 * User: nkolosov
 * Date: 3/9/15
 * Time: 10:23 PM
 */

namespace app\controllers;

use app\models\User;
use yii\base\Exception;
use yii\rest\Controller;

class InstallController extends Controller {
    public function actionIndex ()
    {
        try {
            if (!User::isTableExist()) {
                return User::createTable();
            }

            return false;
        } catch (Exception $e) {
            return $e->getMessage();
        }
    }
} 