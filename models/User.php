<?php

namespace app\models;

use yii\db\ActiveRecord;

class User extends ActiveRecord
{
    public static function primaryKey()
    {
        return ['id'];
    }

    public static function isTableExist()
    {
        \Yii::$app->db->createCommand()->dropTable('user')->execute();
        $tableNames = \Yii::$app->db->schema->tableNames;

        return in_array(static::tableName(), $tableNames);
    }

    public static function createTable()
    {
        $columns = [
            'id'         => 'INTEGER PRIMARY KEY NOT NULL',
            'first_name' => 'CHAR(50) NOT NULL',
            'last_name'  => 'CHAR(50) NOT NULL',
        ];

        return \Yii::$app->db->createCommand()->createTable(static::tableName(), $columns)->execute();
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'user';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['first_name', 'last_name'], 'required'],
            [['first_name', 'last_name'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'first_name' => 'First Name',
            'last_name' => 'Last Name',
        ];
    }
}
