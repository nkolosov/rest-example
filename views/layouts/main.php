<?php
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use app\assets\AppAsset;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>

<?php $this->beginBody() ?>
    <div class="wrap">
        <div class="container">
            <?= $content ?>
        </div>
    </div>

    <footer class="footer">
        <div class="container">
            <p class="pull-left">&copy; My Company <?= date('Y') ?></p>
            <p class="pull-right"><?= Yii::powered() ?></p>
        </div>
    </footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
<script type="text/javascript">
    $(document).ready(function() {
        $('#edit').on('click', function() {
            var id = prompt('Enter ID:', '');

            if (!id || id == '0') {
                alert('You need enter ID!');
                return false;
            }

            $.ajax({
                'url' : '/rest/users/' + id,
                'method' : 'GET',
                'context' : this,
                'success' : function (user) {
                    if (user) {
                        var defaultName = user.first_name;
                        var defaultLast = user.last_name;

                        var name = prompt('Enter First Name:', defaultName);

                        if (!name || name == '') {
                            alert('You need enter First Name!');
                            return;
                        }

                        var last = prompt('Enter Last Name:', defaultLast);

                        if (!last || last == '') {
                            alert('You need enter Last Name!');
                            return;
                        }

                        $.ajax({
                            'url' : '/rest/users/' + id,
                            'data' : 'first_name=' + name + '&last_name=' + last,
                            'method' : 'PUT',
                            'success' : function () {
                                alert('Record has been successfully updated!');
                            }
                        });

                        $('#all').trigger('click');
                    } else {
                        alert('Record ' + id + ' not found!');
                    }
                }
            });

            return false;
        });

        $('#create').on('click', function() {
            var name = prompt('Enter First Name:', '');

            if (!name || name == '') {
                alert('You need enter First Name!');
                return false;
            }

            var last = prompt('Enter Last Name:', '');

            if (!last || last == '') {
                alert('You need enter Last Name!');
                return false;
            }

                $.ajax({
                    'url' : '/rest/users',
                    'data' : 'first_name=' + name + '&last_name=' + last,
                    'method' : 'POST',
                    'success' : function () {
                        alert('Record has been successfully saved!');
                    }
                });

            $('#all').trigger('click');
            return false;
        });

        $('#get').on('click', function() {
            var id = prompt('Enter ID:', 0);

            if (!id || id == '0') {
                alert('You need enter ID');
                return false;
            } else {
                $.ajax({
                    'url' : '/rest/users/' + id,
                    'method' : 'GET',
                    'success' : function (user) {
                        if (response) {
                            $('#content').html('ID: ' + user.id + ' | First Name: ' + user.first_name + ' | Last Name: ' + user.last_name);
                        } else {
                            alert('Record ' + id + ' not found!');
                        }
                    }
                });
            }

            return false;
        });

        $('#remove').on('click', function() {
            var id = prompt('Enter ID:', 0);

            if (!id || id == '0') {
                alert('You need enter ID');
                return false;
            } else {
                $.ajax({
                    'url' : '/rest/users/' + id,
                    'method' : 'DELETE',
                    'success' : function (response) {
                        if (response) {
                            alert(response.message);
                        } else {
                            alert('Record ' + id + ' successfully removed!');
                        }
                    }
                });
            }

            $('#all').trigger('click');
            return false;
        });

        $('#all').on('click', function() {
            $.ajax({
                'url' : '/rest/users',
                'method' : 'GET',
                'success' : function (response) {
                    var result = '';

                    $.each(response, function (key, user) {
                        var additional = 'ID: ' + user.id + ' | First Name: ' + user.first_name + ' | Last Name: ' + user.last_name;

                        result = result + additional + '<br />';
                    });

                    $('#content').html(result);
                   // console.log($.parseJSON(response));
                }
            });

            return false;
        });
    });
</script>
