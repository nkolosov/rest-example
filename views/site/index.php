<?php
/* @var $this yii\web\View */
$this->title = 'My Yii Application';
?>
<div class="site-index">

    <div class="body-content">

        <div class="row">
            <div class="col-md-4">
                <p><a class="btn btn-default" href="#" id="all">Get All</a></p>
                <p><a class="btn btn-default" href="#" id="get">Get</a></p>
                <p><a class="btn btn-default" href="#" id="create">Create</a></p>
                <p><a class="btn btn-default" href="#" id="edit">Edit</a></p>
                <p><a class="btn btn-default" href="#" id="remove">Remove</a></p>
            </div>
            <div class="col-md-8" id="content">

            </div>
        </div>

    </div>
</div>